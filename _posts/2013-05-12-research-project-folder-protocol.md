---
layout: post
title: PRPF - A protocol for research project folder
quality: 2
category: miscellaneous
published: true
comments: true
tags: [protocol]
---

Research projects are not like engineering projects, they have to deal with more uncertainties as research is like "experiments on top of experiments", so the project folder may get messy and becomes difficult for researchers to continue as the research progress unfolds. To deal with this kind of situations, this post announces a protocol for better organize project folder: PRPF, a short for a Protocol for Research Project Folder.

The philosophy behind PRPF includes:

* Open research, the codes should be released along with paper in most cases.
* The results can be reproduced by other people.
* Easy to understand, debug, repeat, etc..


PRPFs (PRPF strict)
------------------
---

### required workspaces (folders) under root workspace:

* **READEME.md**: global instructions for the project, could simply re-direct to comments folder.

* **code**: which keeps most codes and scripts, should be kept the way ready to published.
	+ README.md
	+ preprocessing
	+ config
	+ tools
	+ utils
	+ XX model
	+ ...
	
* **data**: whichs keeps most data, including original data, raw data, preprocessed data, feature data (if too trivial, could be kept in exp folder), etc. Note that data analysis should not be kept here, it should be kept in comments folder. Also note that in PRPFs, all other data should be generated from original data, the code for generating other data from original data can be kept in code/preprocessing.
	+ README.md
	+ original
	+ raw
	+ feature
	+ ...
	
* **exp**: experiment directly related enviroment setup, data, scripts, resources, results, etc. Note that exp analysis should not be kept here, it should be kept in comments folder.
	+ README.md
	+ xx-exp
	+ env
	+ features
	+ resources
	
* **script**: paper.

* **comments**: analysis of data and exp, ideas, comments, such as TODO, etc.

* **refs**: references, related works, and some comments towards them.

### optional：

* **doc**: some documents for the project.

* **readytodelete**: cache, buffer, recycle bin.


<br/>

PRPFl (PRPF loose)
-----------------
---

In PRPFl:

* The code workspace could be extracted into root workspace.
* In data folder, it's not required that all data should be generated from original data and the code should be kept in the code folder.

