---
layout: post
title: A survey of data mining and machine learning contest
categories: [machine learning, data mining, contest]
tags: [competition]
quality: 2
published: pre
permalink: /pre/contest
comments: true
---

Competition in computer science has a long history, ACM-ICPC may be the most well known contest in the field. Recent years, many data mining and machine learning contests have emerged very fast, many of them are quite interesting and provide a good practice for beginners and specialists to learn and study different problems and approaches. Through these competitions, some methodologies and approaches were proved to be very powerful, such as feature engineerings and model ensemble methodologies, and latent factor model approaches, etc. As I am a rookie to machine learning and data mining, I want to learn via these contests as there are many papers (workshops) and codes on some of them, this post wants to provide a roughly organization of these contests. Note that I don't want to cover as much as possible, this post will without doubt be biased by my interests and view of point.

KDD Cups and Link Prediction
-----------------------------

KDD Cups has been holed every year since 1997, where here is a [webpage][kddcuphistory] expand a access. Let's mainly review recent two years KDD Cups:

[KDD Cups 2012][kddcup2012] has two tracks, namely, i) rank the recommend list for a user in microblog, and ii) click-through rate prediction. There are multiple and rich data sources in the data sets, and the data contains sparse entries, also the data is dynamic. All of these features form solid challanges for the tasks.

[KDD Cups 2013][kddcup2013] has two tracks, namely, i) rank paper list for each author (put those papers written by the author in front), and 2) identify same author in publication network, two tasks have same data source. The data sets also contain multiple and rich data sources, 

Both track 1 of KDD Cup 2012&2013 are under the categories of the link prediction (in heterogeneous network), under this category, there are many contests, such as [Facebook recruiting contest][fb2012], and a big subcategory is item (classically, movie) recommendation([KDD Cups 2007 and Netflix][kddcup2007netflix]). Link prediction is a fundamental problem in network, through these competitions, a roughly understanding towards this topic could be built.

Representation Learning
------------------------

Representation Learning (currently dominated by Deep Learning) became popular in the past few years. There are many image/audio classification competitions are ruled by deep learning, such as several competitions of ICML 2013 (in [Kaggle][kaggle]).


Websites and Workshops
-----------------------
While there are many websites for traditional competitions (mostly algorithmic, known as online judge), there are much more fewer websites for contests in the field of data mining and machine learning. Most recommended website for data competitions is [Kaggle][kaggle], there are many contests are hold there, it's a wonderful site. Other sites, such as [chalearn][], are also worth visiting.

On the other hand, many competitions have workshops which published approaches of the winers, but it seems that at this moment, they are mostly distributed over all Internet, not even linked/included by Kaggle. So in [here][workshopshare] I share a small repository of workshop papers, which include all accessible workshop papers/slides related on this post.


[kddcuphistory]: http://www.kdd.org/kddcup/index.php
[kddcup2012]: https://www.kddcup2012.org/
[kddcup2013]: http://www.kaggle.com/c/kdd-cup-2013-author-paper-identification-challenge
[fb2012]: https://www.kaggle.com/c/FacebookRecruiting
[kddcup2007netflix]: http://www.cs.uic.edu/~liub/Netflix-KDD-Cup-2007.html
[kaggle]: https://www.kaggle.com
[workshopshare]: https://www.dropbox.com/sh/u5q3dat2smklb3z/fYLbzheVZK
[chalearn]: http://www.chalearn.org/index.html
