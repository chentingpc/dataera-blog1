---
layout: post
title: Test of Dense Matrix in C++ using 2d Array and Eigen
categories: [programming]
tags: [fast]
published: true
quality: 2
comments: true
---

{{ post.title }}

I need to use matrix data structure in my program, while C++ has 2d arrays, it's very low level. On the other hand, some libraries like Eigen provide a higher level of matrix data structure. But in my opinion, no matter how good a library performs in some high-skilled operations such as SVD, the fast speed on basic operations, including read(access), write, sum, dot, should be a requisite condition of such libraries. Because in real applications, such basic operations may be much more frequent than those high-skilled ones, if a library is slow on such operations, it may turn out to be a burden or even bottleneck of the system.

So I write some very simple programs using both 2d array and Eigen3 dense matrix (MatrixXf), and compare their performances on 4 basic operations. I list some of my test result below:

(Note: the three kind of access are tested, default is including both m[i][j] and m[j][i] in outside-i-inside-j loop, the seq only includes m[i][j], the proc is include more non-related operations in seq, the details can be found on the test code in appendix. Also note that here 2d array is row-based, while Eigen is column-based.)

	**10000X10000 matrix, compile command: g++ -o test.o test.cpp -O0**

	Eigen:
	[!COST] init:		5.79 sec.
	[!COST] read:		13.37 sec.
	[!COST] read(seq):	5.13 sec.
	[!COST] read(proc):	10.14 sec.
	[!COST] write:		21.64 sec.
	[!COST] sum:		1.96 sec.
	[!COST] dot:		1.97 sec.
	CPP:
	[!COST] init:		1.73 sec.
	[!COST] read:		2.38 sec.
	[!COST] read(seq):	0.88 sec.
	[!COST] read(proc):	5.94 sec.
	[!COST] write:		3.31 sec.
	[!COST] sum:		0.63 sec.
	[!COST] dot:		0.52 sec.

	**10000X10000 matrix, compile command: g++ -o test.o test.cpp -O1**

	Eigen:
	[!COST] init:		1.69 sec.
	[!COST] read:		1.96 sec.
	[!COST] read(seq):	0.64 sec.
	[!COST] read(proc):	5.06 sec.
	[!COST] write:		2.55 sec.
	[!COST] sum:		0.06 sec.
	[!COST] dot:		0.1 sec.
	CPP:
	[!COST] init:		1.53 sec.
	[!COST] read:		1.99 sec.
	[!COST] read(seq):	0.65 sec.
	[!COST] read(proc):	6.76 sec.
	[!COST] write:		2.17 sec.
	[!COST] sum:		0.41 sec.
	[!COST] dot:		0.14 sec.
	
	**10000X10000 matrix, compile command: g++ -o test.o test.cpp -O2**
	
	Eigen:
	[!COST] init:		1.62 sec.
	[!COST] read:		1.94 sec.
	[!COST] read(seq):	0.37 sec.
	[!COST] read(proc):	5.51 sec.
	[!COST] write:		2.03 sec.
	[!COST] sum:		0.05 sec.
	[!COST] dot:		0.06 sec.
	CPP:
	[!COST] init:		1.63 sec.
	[!COST] read:		2.04 sec.
	[!COST] read(seq):	0.37 sec.
	[!COST] read(proc):	5.48 sec.
	[!COST] write:		2.23 sec.
	[!COST] sum:		0.15 sec.
	[!COST] dot:		0.1 sec.

	**10000X10000 matrix, compile command: g++ -o test.o test.cpp -O3**

	Eigen:
	[!COST] init:		1.61 sec.
	[!COST] read:		1.93 sec.
	[!COST] read(seq):	0.38 sec.
	[!COST] read(proc):	5.28 sec.
	[!COST] write:		2.04 sec.
	[!COST] sum:		0.06 sec.
	[!COST] dot:		0.06 sec.
	CPP:
	[!COST] init:		1.58 sec.
	[!COST] read:		2.01 sec.
	[!COST] read(seq):	0.37 sec.
	[!COST] read(proc):	5.25 sec.
	[!COST] write:		2.17 sec.
	[!COST] sum:		0.15 sec.
	[!COST] dot:		0.05 sec.


**Conclusion**

To make Eigen comparable/faster than 2d array, you need to turn on at least GCC/G++ O1 optimization option, and if you turn on 02, the results reveal that Eigen won over 2d array in several ways, at least comparable. Using O3 option can further speed up both of them. Also, using -msse2 in compile command didn't help.

**Appendix**

_test.cpp_

{% highlight c++ %}
#include <iostream>
#include <Eigen/Dense>
#include <ctime>
using Eigen::MatrixXf;

inline void plus(float &dst, float &src)
{
	dst += src * dst;
}

inline void process(float &dst, float &src)
{
	float tmp = src;
	int tmp2 = rand()%1000, tmp3 = rand()%1000, tmp4 = rand()%1000;
	if (tmp2 > tmp3 && tmp3 > tmp4)
		dst += tmp + tmp2 - tmp3;
	else
		dst += tmp + tmp3 - tmp2;
}

inline int cpp_testor_read(float **m, const int M, const int N, int choice=1)
{
	float randomTmp = 0;
	switch(choice)
	{
		case 1:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type1: both way
					randomTmp += m[i][j];
					randomTmp -= m[j][i];
				}
			break;
		case 2:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type2: single way
					plus(randomTmp, m[i][j]);
				}
			break;
		default:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type3: single way with more process
					process(randomTmp, m[i][j]);
				}
			break;

	}
	return randomTmp;
}

inline int eigen_testor_read(MatrixXf &m, const int M, const int N, int choice=1)
{
	float randomTmp = 0;
	switch(choice)
	{
		case 1:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type1: both way
					randomTmp += m(i, j);
					randomTmp -= m(j, i);
				}
			break;
		case 2:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type2: single way
					plus(randomTmp, m(j, i));
				}
			break;
		default:
			for (int i = 0; i < M; i ++)
				for (int j = 0; j < N; j ++)
				{
					// type3: single way with more process
					process(randomTmp, m(j, i));
				}
			break;

	}
	return randomTmp;
}

inline int cpp_testor_write(float **m, const int M, const int N)
{
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
		{
			// type1: both way
			m[i][j] += m[j][i];
			m[j][i] -= m[i][j];
		}
	return m[rand()%10000][rand()%10000];
}

inline int eigen_testor_write(MatrixXf &m, const int M, const int N)
{
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
		{
			// type1: both way
			m(i, j) += m(j, i);
			m(j, i) -= m(i, j);
		}
	return m(rand()%10000, rand()%10000);
}

inline int cpp_testor_sum(float **m, const int M, const int N)
{
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
		{
			m[i][i] += m[i][j];
		}
	return m[rand()%1000][rand()%1000];
}

inline int eigen_testor_sum(MatrixXf &m, const int M, const int N)
{
	m += m;
	return m(0, 0);
}

inline int cpp_testor_dot(float **m, const int M, const int N, float val)
{
	float randomTmp = 0;
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
		{
			m[i][j] *= val;
		}
	return m[rand()%1000][rand()%1000];
}

inline int eigen_testor_dot(MatrixXf &m, const int M, const int N, float val)
{
	m *= val;
	return m(0, 0);
}

float** cpp_generator_mtarix(const int M, const int N)
{
	float **m = new float*[M];
	for (int i = 0; i < M; i ++)
		m[i] = new float[N];
	return m;
}

MatrixXf& eigen_generator_matrix(const int M, const int N)
{

	static MatrixXf m(M,N);
	return m;
}

int main()
{
	const int M = 10000;
	const int N = M;
	int antiopt = 0;
	srand(time(NULL));
	float val = rand()%10000 + 1;
	std::cout<< M << " " << N << std::endl;

	std::cout<<"Eigen:" << std::endl;
	size_t t = clock();
	//MatrixXf m = eigen_generator_matrix(M, N);
	MatrixXf m(M,N);
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
			m(j,i) = rand()%1000 + 1;
	t = clock() - t;
	std::cout<< "[!COST] init:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_read(m,M,N);
	t = clock() - t;
	std::cout<< "[!COST] read:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_read(m,M,N,2);
	t = clock() - t;
	std::cout<< "[!COST] read(seq):\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_read(m,M,N,3);
	t = clock() - t;
	std::cout<< "[!COST] read(proc):\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_write(m,M,N);
	t = clock() - t;
	std::cout<< "[!COST] write:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_sum(m,M,N);
	t = clock() - t;
	std::cout<< "[!COST] sum:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += eigen_testor_dot(m,M,N, val);
	t = clock() - t;
	std::cout<< "[!COST] dot:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	std::cout<<"CPP:" << std::endl;
	t = clock();
	//float **mm = cpp_generator_mtarix(M, N);
	float **mm = new float*[M];
	for (int i = 0; i < M; i ++)
		mm[i] = new float[N];
	for (int i = 0; i < M; i ++)
		for (int j = 0; j < N; j ++)
			mm[i][j] = rand()%1000 + 1;
	t = clock() - t;
	std::cout<< "[!COST] init:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_read(mm,M,N);
	t = clock() - t;
	std::cout<< "[!COST] read:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_read(mm,M,N,2);
	t = clock() - t;
	std::cout<< "[!COST] read(seq):\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_read(mm,M,N,3);
	t = clock() - t;
	std::cout<< "[!COST] read(proc):\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_write(mm,M,N);
	t = clock() - t;
	std::cout<< "[!COST] write:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_sum(mm,M,N);
	t = clock() - t;
	std::cout<< "[!COST] sum:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	t = clock();
	antiopt += cpp_testor_dot(mm,M,N, val);
	t = clock() - t;
	std::cout<< "[!COST] dot:\t\t" << t/float(CLOCKS_PER_SEC) << " sec." <<std::endl;

	std::cout<<antiopt<<std::endl;
}
{% endhighlight %}
